Changelog
#########

attotimebuilder 0.4.2-dev.0
===========================

*Release date: YYYY-MM-DD*

Changed
-------
* Change tests to account for 1 and 3 digit years in dates not being allowed by aniso8601 parser
* Specify changelog location in :code:`setup.py`

attotimebuilder 0.4.1
=====================

*Release date: 2025-01-09*

Added
-----
* Development requirements handled by :code:`extras_require` (install with :code:`pip install -e .[dev]`)
* Pre-commit hooks, managed with `pre-commit <https://pre-commit.com/>`_ (install with :code:`pre-commit install`)
* Add :code:`readthedocs.yaml` to make configuration explicit

Changed
-------
* Code formatted with `Black <https://black.readthedocs.io/en/stable/index.html>`_
* Imports sorted with `isort <https://pycqa.github.io/isort/>`_
* Following `Keep a Changelog <https://keepachangelog.com/en/1.0.0/>`_ for this and future CHANGELOG entries
* Heading level of top of CHANGELOG
* Increase supported aniso8601 version to <11.0.0

attotimebuilder 0.4.0
=====================

*Release date: 2021-02-18*

Changes
-------
* Add support for concise interval format (e.g. "2007-12-14T13:30/15:30")
* Implement range checks supported by aniso8601 9.0.0
* Add version to :code:`version.py`
* Cleaner reading of `README.rst` into the :code:`long_description` field of `setup.py`
* Define :code:`long_description_content_type` as :code:`text/x-rst`
* Simplify Sphinx configuration
* Bump copyright date to 2021
* Require latest attotime (0.2.2)

Deprecation
-----------
* Deprecate running tests with :code:`python setup.py tests` as the test suite support in Setuptools is `deprecated <https://github.com/pypa/setuptools/issues/1684>`_

attotimebuilder 0.3.1
=====================

*Release date: 2019-09-11*

Changes
-------
* Add support for `aniso8601 <https://bitbucket.org/nielsenb/aniso8601>`_ 8.0.0
* Fix semver usage for prelease version, as required by `clause 9 <https://semver.org/#spec-item-9>`_

attotimebuilder 0.3.0
=====================

*Release date: 2019-06-11*

Changes
-------
* Add support for `aniso8601 <https://bitbucket.org/nielsenb/aniso8601>`_ 7.0.0
* Promote to Beta

attotimebuilder 0.2.1
=====================

*Release date: 2019-03-08*

Changes
-------
* Add support for `aniso8601 <https://bitbucket.org/nielsenb/aniso8601>`_ 6.0.0

attotimebuilder 0.2.0
=====================

*Release date: 2019-03-01*

Changes
-------
* Update to aniso8601 >=5.0.0
* When :code:`build_time` is called with only :code:`hh` 24<=hh<25, a :code:`MidnightBoundsError` is raised, this used to be a :code:`HoursOutOfBoundsError`
* Promote interval components to :code:`attodatetime` objects if the given duration has second, microsecond, or nanosecond resolution, or if the duration tuple has hour, minute, or second components

  - Before promotion would only happen if the duration tuple had hour, minute, or second components

attotimebuilder 0.1.0
=====================

*Release date: 2019-01-09*

Changes
-------
* Update to attotime >=0.2.0
* Remove support for distutils
* Run tests with setuptools
* Initial Read the Docs support

attotimebuilder 0.0.3
=====================

*Release date: 2019-01-05*

Changes
-------
* Use ranges for install_requires
* Update to attotime >=0.1.3
* Add MANIFEST.in
* Make tests package importable

attotimebuilder 0.0.2
=====================

*Release date: 2018-10-25*

Changes
-------
* Fix project URL

attotimebuilder 0.0.1
=====================

*Release date: 2018-10-25*

Changes
-------
* Initial release
